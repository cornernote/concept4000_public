<?php

$yii = dirname(dirname(__FILE__)) . '/vendors/yii/yii-1.1.8.r3324/framework/yii.php';
$globals = dirname(dirname(__FILE__)) . '/protected/globals.php';
$config = dirname(dirname(__FILE__)) . '/protected/config/main.php';

defined('YII_DEBUG') or define('YII_DEBUG', false);
//defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($yii);
require_once($globals);
Yii::createWebApplication($config)->run();