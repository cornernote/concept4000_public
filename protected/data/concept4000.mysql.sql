CREATE TABLE tbl_serial_log (
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    request TEXT,
    response TEXT,
    created VARCHAR(128) NOT NULL
);
CREATE TABLE tbl_setting (
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    key VARCHAR(128) NOT NULL,,
    value TEXT
);
CREATE TABLE tbl_user (
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(128) NOT NULL,
    password VARCHAR(128) NOT NULL,
    email VARCHAR(128)
);
INSERT INTO tbl_user VALUES(1,'admin','5ecc048bdfc2223a5149831160321fe88119cf19:12','admin@localhost');
CREATE TABLE tbl_zone (
    id INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(128) NOT NULL,
    type VARCHAR(32) NOT NULL,
    ref INT(11) NOT NULL,
    status INT(11),
    title VARCHAR(128),
    hidden INT(11)
);