CREATE TABLE tbl_serial_log (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    request TEXT,
    response TEXT, 
    created TEXT
);
CREATE TABLE tbl_setting (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    key VARCHAR(128) NOT NULL,
    value TEXT
);
CREATE TABLE tbl_user (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    username VARCHAR(128) NOT NULL,
    password VARCHAR(128) NOT NULL,
    email VARCHAR(128)
);
INSERT INTO tbl_user VALUES(1,'admin','5ecc048bdfc2223a5149831160321fe88119cf19:12','admin@localhost');
CREATE TABLE tbl_zone (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(128) NOT NULL,
    type VARCHAR(32) NOT NULL,
    ref NUMERIC NOT NULL,
    status NUMERIC,
    title VARCHAR(128),
    hidden NUMERIC
);